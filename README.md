# easy-rotas-pos

## Autenticação

Método: POST<br>
Url: pdv/auth<br>

Exemplo:

```
localhost:3333/pdv/auth

body: {
  "serial_number": "123"
}
```

Callback:

```typescript
{
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2NzU0NTM3MTcsImV4cCI6MTY3NjA1ODUxNywic3ViIjoiMTIifQ.Y3Ts5u7eEfeKIfBkj78ObBEs70dldkbURm0oIBvRYwU"
}
```

## Autenticação com email e senha
### Após esse login, substituir o novo token pelo anterior

Método: POST<br>
Url: pdv/login<br>
Headers: { Authorization: "Bearer token" } // Token recebido na rota pdv/auth

Exemplo:

```
localhost:3333/pdv/login

body: {
  "email": "email@mail.com",
  "password": "senha123"
}
```

Callback:

```typescript
{
  "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpYXQiOjE2NzU0NTM3MTcsImV4cCI6MTY3NjA1ODUxNywic3ViIjoiMTIifQ.Y3Ts5u7eEfeKIfBkj78ObBEs70dldkbURm0oIBvRYwU"
}
```

## Buscar eventos

Método: GET<br>
Url: pdv/events<br>
Headers: { Authorization: "Bearer token" }

Callback de exemplo:

```typescript
[
  {
    "id": 1,
    "name": "Teste integração com a pos",
    "due_date": "2023-09-28T06:00:00.000Z",
    "age_classification": 18,
    "banner": "http://localhost:3333/uploads/images/1677676047943-rPWKVlTgQd9of5Tui2dP7lL8B1qVwu.jpeg",
    "address": {
      "name": "local x",
      "image": "http://localhost:3333/uploads/images/1677676047943-rPWKVlTgQd9of5Tui2dP7lL8B1qVwu.jpeg",
      "street": "Avenida Ivo Guersoni",
      "number": "124",
      "complement": "1234",
      "district": "Vila Beatriz",
      "city": "Pouso Alegre",
      "uf": "MG",
      "zipcode": "37555600",
      "lat": "3233323",
      "lon": "32332323"
    }
  },
  {
    "id": 5,
    "name": "CAMAROTE AVENIDA - CARNAVAL ATRASADO DE SÃO PAULO",
    "due_date": "2023-06-20T20:00:00.000Z",
    "age_classification": 18,
    "banner": "http://localhost:3333/uploads/images/1677676047943-rPWKVlTgQd9of5Tui2dP7lL8B1qVwu.jpeg",
    "address": {
      "name": "100- Holiday Inn parque Anhemb",
      "image": "http://localhost:3333/uploads/images/1675863762785-B8b1QxEbIZ4M9W9zHMixsXuWSHvFry.jpeg",
      "street": "Avenida Olavo Fontoura",
      "number": "1209",
      "complement": null,
      "district": "Parque Anhembi",
      "city": "São Paulo",
      "uf": "SP",
      "zipcode": "02012-021",
      "lat": "-23.516244",
      "lon": "-46.643008"
    }
  }
]
```

## Buscar evento

Método: GET<br>
Url: pdv/events/:id<br>
Headers: { Authorization: "Bearer token" }

Callback de exemplo:

```typescript
{
  "id": 5,
  "name": "CAMAROTE AVENIDA - CARNAVAL ATRASADO DE SÃO PAULO",
  "ticket_name": "CARNAVAL ATRASADO DE SÃO PAULO",
  "due_date": "2023-06-20T20:00:00.000Z",
  "alternative_date_start": "2023-06-20T06:00:00.000Z",
  "alternative_date_end": "2023-06-25T06:00:00.000Z",
  "sales_start_date": "2023-02-23T06:00:00.000Z",
  "sales_end_date": "2023-06-19T06:00:00.000Z",
  "ticket_phrase": "Lorem ipsum.",
  "description": "Lorem ipsum.",
  "age_classification": 18,
  "banner": "http://localhost:3333/uploads/images/1677676047943-rPWKVlTgQd9of5Tui2dP7lL8B1qVwu.jpeg",
  "about": "Lorem ipsum.",
  "event_site": "https://parque-Anhemb.com",
  "info": "Lorem ipsum.",
  "category": {
    "id": 3,
    "name": "Festas",
    "slug": "festas"
  },
  "address": {
    "name": "100- Holiday Inn parque Anhemb",
    "image": "http://localhost:3333/uploads/images/1675863762785-B8b1QxEbIZ4M9W9zHMixsXuWSHvFry.jpeg",
    "street": "Avenida Olavo Fontoura",
    "number": "1209",
    "complement": null,
    "district": "Parque Anhembi",
    "city": "São Paulo",
    "uf": "SP",
    "zipcode": "02012-021",
    "lat": "-23.516244",
    "lon": "-46.643008"
  },
  "ticket_classes": [
    {
      "id": 3,
      "name": "Pista",
      "type": "normal",
      "ticket_blocks": [
        {
          "id": 34,
          "name": "final",
          "subtotal": 3443.43,
          "tax_value": 344.34,
          "total": 3787.77,
          "type": "hall",
          "expires_at": "2023-03-03T15:17:02.000Z"
        }
      ]
    },
    {
      "id": 12,
      "name": "teste meia entrada",
      "type": "normal",
      "ticket_blocks": [
        {
          "id": 26,
          "name": "lote antecipado",
          "subtotal": 100,
          "tax_value": 10,
          "total": 110,
          "type": "hall",
          "expires_at": "2023-03-03T13:40:36.000Z"
        }
      ]
    }
  ]
}
```

- Caso a função da pos esteja para vendas em portaria, alem do objeto acima é recebido os ingressos disponíveis para venda.


```typescript
{
  "id": 5,
  "name": "CAMAROTE AVENIDA - CARNAVAL ATRASADO DE SÃO PAULO",
  "ticket_name": "CARNAVAL ATRASADO DE SÃO PAULO",
  "due_date": "2023-06-20T20:00:00.000Z",
  "alternative_date_start": "2023-06-20T06:00:00.000Z",
  "alternative_date_end": "2023-06-25T06:00:00.000Z",
  "sales_start_date": "2023-02-23T06:00:00.000Z",
  "sales_end_date": "2023-06-19T06:00:00.000Z",
  "ticket_phrase": "Lorem ipsum.",
  "description": "Lorem ipsum.",
  "age_classification": 18,
  "banner": "http://localhost:3333/uploads/images/1677676047943-rPWKVlTgQd9of5Tui2dP7lL8B1qVwu.jpeg",
  "about": "Lorem ipsum.",
  "event_site": "https://parque-Anhemb.com",
  "info": "Lorem ipsum.",
  "category": {
    "id": 3,
    "name": "Festas",
    "slug": "festas"
  },
  "address": {
    "name": "100- Holiday Inn parque Anhemb",
    "image": "http://localhost:3333/uploads/images/1675863762785-B8b1QxEbIZ4M9W9zHMixsXuWSHvFry.jpeg",
    "street": "Avenida Olavo Fontoura",
    "number": "1209",
    "complement": null,
    "district": "Parque Anhembi",
    "city": "São Paulo",
    "uf": "SP",
    "zipcode": "02012-021",
    "lat": "-23.516244",
    "lon": "-46.643008"
  },
  "ticket_classes": [
    {
      "id": 3,
      "name": "Pista",
      "type": "normal",
      "ticket_blocks": [
        {
          "id": 34,
          "name": "final",
          "subtotal": 3443.43,
          "tax_value": 344.34,
          "total": 3787.77,
          "type": "hall",
          "expires_at": "2023-03-03T15:17:02.000Z"
        }
      ]
    },
    {
      "id": 12,
      "name": "teste meia entrada",
      "type": "normal",
      "ticket_blocks": [
        {
          "id": 26,
          "name": "lote antecipado",
          "subtotal": 100,
          "tax_value": 10,
          "total": 110,
          "type": "hall",
          "expires_at": "2023-03-03T13:40:36.000Z"
        }
      ]
    }
  ],
  "tickets": [
    {
      "id": 470,
      "code": "YQCWACMTNYL9",
      "subtotal": 100,
      "tax_value": 10,
      "total": 110,
      "ticket_data": {
        "name": null,
        "email": null,
        "phone": null,
        "cpf_rg": null
      },
      "status": "offline",
      "class_id": 12,
      "block_id": 26,
      "event_id": 5
    },
    {
      "id": 471,
      "code": "KX21ADSHP7NY",
      "subtotal": 100,
      "tax_value": 10,
      "total": 110,
      "ticket_data": {
        "name": null,
        "email": null,
        "phone": null,
        "cpf_rg": null
      },
      "status": "offline",
      "class_id": 12,
      "block_id": 26,
      "event_id": 5
    },
    {
      "id": 472,
      "code": "ED41HBRT6DJ4",
      "subtotal": 100,
      "tax_value": 10,
      "total": 110,
      "ticket_data": {
        "name": null,
        "email": null,
        "phone": null,
        "cpf_rg": null
      },
      "status": "offline",
      "class_id": 12,
      "block_id": 26,
      "event_id": 5
    },
    {
      "id": 473,
      "code": "MLPF6SBOD8P4",
      "subtotal": 100,
      "tax_value": 10,
      "total": 110,
      "ticket_data": {
        "name": null,
        "email": null,
        "phone": null,
        "cpf_rg": null
      },
      "status": "offline",
      "class_id": 12,
      "block_id": 26,
      "event_id": 5
    }
  ]
}
```


## Reservar ingressos

Método: POST<br>
Url: pdv/sales<br>
Headers: { Authorization: "Bearer token" }

Body de exemplo:

payment_method:
- debit_card
- credit_card
- money
- pix

Pagamentos em dinheiro não necessitam ser confirmadas na proxima rota

```typescript
{
  "origin": "pos",
  "payment_method": "debit_card" | "credit_card" | "money" | "pix",
  "client_cpf": "12345678901",
  "items": [
    {
      "block_id": 1,
      "ageOfMajority": true // obrigatório se age_classification do evento > 18
    },
    {
      "block_id": 1,
      "ageOfMajority": true // obrigatório se age_classification do evento > 18
    }
  ]
}
```

Callback:

```typescript
{
  "data": {
    "id": 18,
    "subtotal": 100,
    "tax_value": 5,
    "client_cpf": "12345678901",
    "total": 105,
    "origin": "pos",
    "status": "pending",
    "reference_id": null,
    "reference_object": null,
    "items": [
      {
        "ticket_id": 31,
        "ticket": {
          "code": "WN9LN0KX61XS",
          "status": "active",
          "created_at": "2023-02-03T19:53:36.000Z",
          "data": {
            "name": null,
            "email": null,
            "phone": null,
            "cpf_rg": null
          },
          "event": {
            "name": "teste",
            "slug": "teste",
            "due_date": "2023-09-01 00:00:00",
            "image": null,
            "city": "Pouso Alegre",
            "uf": "MG"
          },
          "ticket_class": {
            "name": "Entrada",
            "slug": "entrada",
            "type": "normal"
          },
          "ticket_block": {
            "name": "1",
            "slug": "1",
            "price": 50
          }
        },
        "subtotal": 50,
        "tax_value": 2.5,
        "total": 52.5
      },
      {
        "ticket_id": 32,
        "ticket": {
          "code": "1996PQA3YRJA",
          "status": "active",
          "created_at": "2023-02-03T19:53:36.000Z",
          "data": {
            "name": null,
            "email": null,
            "phone": null,
            "cpf_rg": null
          },
          "event": {
            "name": "teste",
            "slug": "teste",
            "due_date": "2023-09-01 00:00:00",
            "image": null,
            "city": "Pouso Alegre",
            "uf": "MG"
          },
          "ticket_class": {
            "name": "Entrada",
            "slug": "entrada",
            "type": "normal"
          },
          "ticket_block": {
            "name": "1",
            "slug": "1",
            "price": 50
          }
        },
        "subtotal": 50,
        "tax_value": 2.5,
        "total": 52.5
      }
    ],
    "created_at": "2023-02-03T19:53:36.000Z",
    "updated_at": "2023-02-03T19:53:36.000Z"
  }
}
```

## Confirmar compra

Método: POST<br>
Url: pdv/sales/:id<br>
Headers: { Authorization: "Bearer token" }

status:
- ok = confirma a venda
- cancel = extorna a venda
- destroy = cancela a venda


```typescript
{
  "status": "ok" | "cancel" | "destroy",
  "reference_id": "id da transação(callback do pagseguro)",
  "reference_object": "objeto da transação(callback do pagseguro)"
}
```

Callback:

```typescript
{
  "data": {
    "id": 18,
    "subtotal": 100,
    "tax_value": 5,
    "client_cpf": "12345678901",
    "total": 105,
    "origin": "pos",
    "status": "processed",
    "reference_id": "id da transação(callback do pagseguro)",
    "reference_object": "objeto da transação(callback do pagseguro)",
    "items": [
      {
        "ticket_id": 31,
        "ticket": {
          "code": "WN9LN0KX61XS",
          "status": "sold",
          "created_at": "2023-02-03T19:53:36.000Z",
          "data": {
            "name": null,
            "email": null,
            "phone": null,
            "cpf_rg": null
          },
          "event": {
            "name": "teste",
            "slug": "teste",
            "due_date": "2023-09-01 00:00:00",
            "image": null,
            "city": "Pouso Alegre",
            "uf": "MG"
          },
          "ticket_class": {
            "name": "Entrada",
            "slug": "entrada",
            "type": "normal"
          },
          "ticket_block": {
            "name": "1",
            "slug": "1",
            "price": 50
          }
        },
        "subtotal": 50,
        "tax_value": 2.5,
        "total": 52.5
      },
      {
        "ticket_id": 32,
        "ticket": {
          "code": "1996PQA3YRJA",
          "status": "sold",
          "created_at": "2023-02-03T19:53:36.000Z",
          "data": {
            "name": null,
            "email": null,
            "phone": null,
            "cpf_rg": null
          },
          "event": {
            "name": "teste",
            "slug": "teste",
            "due_date": "2023-09-01 00:00:00",
            "image": null,
            "city": "Pouso Alegre",
            "uf": "MG"
          },
          "ticket_class": {
            "name": "Entrada",
            "slug": "entrada",
            "type": "normal"
          },
          "ticket_block": {
            "name": "1",
            "slug": "1",
            "price": 50
          }
        },
        "subtotal": 50,
        "tax_value": 2.5,
        "total": 52.5
      }
    ],
    "created_at": "2023-02-03T19:53:36.000Z",
    "updated_at": "2023-02-03T19:55:47.000Z"
  }
}
```
Callback se cancelado ou negado

```typescript
{
  "data": {
    "id": 42,
    "subtotal": 100,
    "tax_value": 5,
    "client_cpf": "12345678901",
    "total": 105,
    "origin": "pos",
    "status": "denied" | "canceled",
    "reference_id": "id da transação(callback do pagseguro)",
    "reference_object": "objeto da transação(callback do pagseguro)",
    "items": [],
    "created_at": "2023-02-06T12:02:28.000Z",
    "updated_at": "2023-02-06T12:07:49.000Z"
  }
}
```

## Buscar vendas

Método: GET<br>
Url: pdv/sales<br>
Headers: { Authorization: "Bearer token" }

Callback de exemplo:

```typescript
{
  "pagination": {
    "total": 3,
    "perPage": 20,
    "page": 1,
    "lastPage": 1
  },
  "data": [
    {
      "id": 5,
      "subtotal": 100,
      "tax_value": 5,
      "client_cpf": "02291097644",
      "total": 105,
      "origin": "pos",
      "status": "pending",
      "reference_id": null,
      "reference_object": null,
      "items": [
        {
          "ticket_id": 5,
          "ticket": null,
          "subtotal": 50,
          "tax_value": 2.5,
          "total": 52.5
        },
        {
          "ticket_id": 6,
          "ticket": null,
          "subtotal": 50,
          "tax_value": 2.5,
          "total": 52.5
        }
      ],
      "created_at": "2023-02-03T13:57:48.000Z",
      "updated_at": "2023-02-03T13:57:48.000Z"
    },
    {
      "id": 17,
      "subtotal": 100,
      "tax_value": 5,
      "client_cpf": "02234675454",
      "total": 105,
      "origin": "pos",
      "status": "pending",
      "reference_id": null,
      "reference_object": null,
      "items": [
        {
          "ticket_id": 29,
          "ticket": null,
          "subtotal": 50,
          "tax_value": 2.5,
          "total": 52.5
        },
        {
          "ticket_id": 30,
          "ticket": null,
          "subtotal": 50,
          "tax_value": 2.5,
          "total": 52.5
        }
      ],
      "created_at": "2023-02-03T14:23:53.000Z",
      "updated_at": "2023-02-03T14:23:53.000Z"
    },
    {
      "id": 18,
      "subtotal": 100,
      "tax_value": 5,
      "client_cpf": "12345678901",
      "total": 105,
      "origin": "pos",
      "status": "processed",
      "reference_id": "id da transação(callback do pagseguro)",
      "reference_object": "objeto da transação(callback do pagseguro)",
      "items": [
        {
          "ticket_id": 31,
          "ticket": null,
          "subtotal": 50,
          "tax_value": 2.5,
          "total": 52.5
        },
        {
          "ticket_id": 32,
          "ticket": null,
          "subtotal": 50,
          "tax_value": 2.5,
          "total": 52.5
        }
      ],
      "created_at": "2023-02-03T19:53:36.000Z",
      "updated_at": "2023-02-03T19:55:47.000Z"
    }
  ]
}
```

## Buscar venda

Método: GET
Url: pdv/sales/:id<br>
Headers: { Authorization: "Bearer token" }

Callback de exemplo:

```typescript
{
  "id": 18,
  "subtotal": 100,
  "tax_value": 5,
  "client_cpf": "12345678901",
  "total": 105,
  "origin": "pos",
  "status": "processed",
  "reference_id": "id da transação(callback do pagseguro)",
  "reference_object": "objeto da transação(callback do pagseguro)",
  "items": [
    {
      "ticket_id": 31,
      "ticket": {
        "code": "WN9LN0KX61XS",
        "status": "active",
        "created_at": "2023-02-03T19:53:36.000Z",
        "data": {
          "name": null,
          "email": null,
          "phone": null,
          "cpf_rg": null
        },
        "event": {
          "name": "teste",
          "slug": "teste",
          "due_date": "2023-09-01 00:00:00",
          "image": null,
          "city": "Pouso Alegre",
          "uf": "MG"
        },
        "ticket_class": {
          "name": "Entrada",
          "slug": "entrada",
          "type": "normal"
        },
        "ticket_block": {
          "name": "1",
          "slug": "1",
          "price": 50
        }
      },
      "subtotal": 50,
      "tax_value": 2.5,
      "total": 52.5
    },
    {
      "ticket_id": 32,
      "ticket": {
        "code": "1996PQA3YRJA",
        "status": "active",
        "created_at": "2023-02-03T19:53:36.000Z",
        "data": {
          "name": null,
          "email": null,
          "phone": null,
          "cpf_rg": null
        },
        "event": {
          "name": "teste",
          "slug": "teste",
          "due_date": "2023-09-01 00:00:00",
          "image": null,
          "city": "Pouso Alegre",
          "uf": "MG"
        },
        "ticket_class": {
          "name": "Entrada",
          "slug": "entrada",
          "type": "normal"
        },
        "ticket_block": {
          "name": "1",
          "slug": "1",
          "price": 50
        }
      },
      "subtotal": 50,
      "tax_value": 2.5,
      "total": 52.5
    }
  ],
  "created_at": "2023-02-03T19:53:36.000Z",
  "updated_at": "2023-02-03T19:55:47.000Z"
}
```

## Sincronizar vendas

Método: POST<br>
Url: pdv/synchronize<br>
Headers: { Authorization: "Bearer token" }

Body de exemplo:

payment_method:
- debit_card
- credit_card
- money
- pix

reference_id: opcional
reference_object: opcional

```typescript
{
  "sales": [
    {
      "origin": "pos",
      "payment_method": "money",
      "client_cpf": "12345678901",
      "reference_id": "id da transação(callback do maquininha)",
      "reference_object": "objeto da transação(callback do maquininha)",
      "items": [
        {
          "ticket_id": 470
        },
        {
          "ticket_id": 471
        }
      ]
    }, {
      "origin": "pos",
      "payment_method": "pix",
      "client_cpf": "12345678901",
      "reference_id": "id da transação(callback do maquininha)",
      "reference_object": "objeto da transação(callback do maquininha)",
      "items": [
        {
          "ticket_id": 472
        },
        {
          "ticket_id": 473
        }
      ]
    }
  ]
}
```

Callback:

```typescript
{
  "message": "Base de dados sincronizada"
}
```

Callback de erro:

```typescript
{
  "message": 'Falha ao sincronizar'
}
```

## Apenas pos autorizadas podem acessar a rota synchronize

Verifique no payload do seu jwt se o boolean synchronize é true ou false

<img src ="./synchronize.png" alt="Imagem do token decodificado" width="100%">
